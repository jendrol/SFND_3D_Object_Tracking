# Final Report

## FP.1 Match 3D Objects

First, multimap prevIDCurrID is createad iterating over matches. In this multimap, key is ID of a bounding box containing query keypoint and value is ID of a bounding box containing query keypoint. In second step, I iterate over bounding boxes in previous frame and find which bounding box in current frame is matched most frequently with this bounding box.
Most probable matches are stored in the bbBestMatches map.

## FP.2 Compute Lidar-based TTC

Influence of outliers in lidar points is suppressed by using median of x-position of points in previous and current frame instead of using points with minimal x-position.

Implementation: src/camFusion_Student.cpp computeTTCLidar

## FP.3 Associate Keypoint Correspondences with Bounding Boxes

In first loop, keypoint matches which train keypoint is inside boundig box ROI are put into the bbMatches vector. In the same loop, euclidean distance between query and train keypoint is computed and stored in the distances vector. The next step filters keypoint matches which distance is far away from median. Remaining keypoint matches are stored in the kptMatches property of bounding box.

Implementation: src/camFusion_Student.cpp clusterKptMatchesWithROI

## FP.4 Compute Camera-based TTC

Implementation uses solution to excercise TTC_camera. Median of ratios of distances between keypoints is used in TTC computation.

Implementation: src/camFusion_Student.cpp computeTTCCamera

## FP.5 Performance Evaluation 1

During implementation and experimention it turned out that TTC is very sensitive to outliers. Using median instead of minimal value seems to be effective in reducing impact of outliers. Mainly in first 18 frames. 

There are some interesting situations in frames after frame 18. In frame 33 we can see outliers caused by reflection from side mirror of preceding vehicle.

![Frame 33](../report_images/LidarFrame33.PNG)  
![Frame 33 Top view](../report_images/LidarFrame33_top.PNG)  

Other source of problem can be situation where lidar points from preceding vehicle are associated with vehicle in right lane.
![Frame 49](../report_images/LidarFrame49.png  "Frame 49")


## FP.6 Performance Evaluation 2


Summary of computed TTCs for each frame and combinations of detector and descriptor is stored in the detectors_descriptors.csv file. Some combinations do not have associated TTC. Reason is that for some combination application crashes.
TTCs vary in orders of seconds to low tens of seconds. Noise is introduced by several factors:
- Detected keypoints are not stable between frames
- Bounding box contains keypoints from vehicle in far front
- There are keypoint mismatches between images

Best performing combinations when compared to lidar-based estimation are:

|Detector|Descriptor|
|--------|----------|
| AKAZE  | AKAZE    |
| SIFT   | SIFT     |
| Shi-Tomasi | BRISK |
