
#include <iostream>
#include <algorithm>
#include <numeric>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "camFusion.hpp"
#include "dataStructures.h"

using namespace std;

double median(std::vector<double> vec) {
    std::sort(vec.begin(), vec.end());
    long medIndex = floor(vec.size() / 2.0);
    double med = vec.size() % 2 == 0 ? (vec[medIndex - 1] + vec[medIndex]) / 2.0 : vec[medIndex];
    return med;
}

// Create groups of Lidar points whose projection into the camera falls into the same bounding box
void clusterLidarWithROI(std::vector<BoundingBox> &boundingBoxes, std::vector<LidarPoint> &lidarPoints, float shrinkFactor, cv::Mat &P_rect_xx, cv::Mat &R_rect_xx, cv::Mat &RT)
{
    // loop over all Lidar points and associate them to a 2D bounding box
    cv::Mat X(4, 1, cv::DataType<double>::type);
    cv::Mat Y(3, 1, cv::DataType<double>::type);

    for (auto it1 = lidarPoints.begin(); it1 != lidarPoints.end(); ++it1)
    {
        // assemble vector for matrix-vector-multiplication
        X.at<double>(0, 0) = it1->x;
        X.at<double>(1, 0) = it1->y;
        X.at<double>(2, 0) = it1->z;
        X.at<double>(3, 0) = 1;

        // project Lidar point into camera
        Y = P_rect_xx * R_rect_xx * RT * X;
        cv::Point pt;
        pt.x = Y.at<double>(0, 0) / Y.at<double>(0, 2); // pixel coordinates
        pt.y = Y.at<double>(1, 0) / Y.at<double>(0, 2);

        vector<vector<BoundingBox>::iterator> enclosingBoxes; // pointers to all bounding boxes which enclose the current Lidar point
        for (vector<BoundingBox>::iterator it2 = boundingBoxes.begin(); it2 != boundingBoxes.end(); ++it2)
        {
            // shrink current bounding box slightly to avoid having too many outlier points around the edges
            cv::Rect smallerBox;
            smallerBox.x = (*it2).roi.x + shrinkFactor * (*it2).roi.width / 2.0;
            smallerBox.y = (*it2).roi.y + shrinkFactor * (*it2).roi.height / 2.0;
            smallerBox.width = (*it2).roi.width * (1 - shrinkFactor);
            smallerBox.height = (*it2).roi.height * (1 - shrinkFactor);

            // check wether point is within current bounding box
            if (smallerBox.contains(pt))
            {
                enclosingBoxes.push_back(it2);
            }

        } // eof loop over all bounding boxes

        // check wether point has been enclosed by one or by multiple boxes
        if (enclosingBoxes.size() == 1)
        { 
            // add Lidar point to bounding box
            enclosingBoxes[0]->lidarPoints.push_back(*it1);
        }

    } // eof loop over all Lidar points
}


void show3DObjects(std::vector<BoundingBox> &boundingBoxes, cv::Size worldSize, cv::Size imageSize, bool bWait)
{
    // create topview image
    cv::Mat topviewImg(imageSize, CV_8UC3, cv::Scalar(255, 255, 255));

    for(auto it1=boundingBoxes.begin(); it1!=boundingBoxes.end(); ++it1)
    {
        // create randomized color for current 3D object
        cv::RNG rng(it1->boxID);
        cv::Scalar currColor = cv::Scalar(rng.uniform(0,150), rng.uniform(0, 150), rng.uniform(0, 150));

        // plot Lidar points into top view image
        int top=1e8, left=1e8, bottom=0.0, right=0.0; 
        float xwmin=1e8, ywmin=1e8, ywmax=-1e8;
        std::vector<double> lidarX;
        for (auto it2 = it1->lidarPoints.begin(); it2 != it1->lidarPoints.end(); ++it2)
        {
            // world coordinates
            float xw = (*it2).x; // world position in m with x facing forward from sensor
            float yw = (*it2).y; // world position in m with y facing left from sensor
            xwmin = xwmin<xw ? xwmin : xw;
            ywmin = ywmin<yw ? ywmin : yw;
            ywmax = ywmax>yw ? ywmax : yw;
            lidarX.push_back(xw);

            // top-view coordinates
            int y = (-xw * imageSize.height / worldSize.height) + imageSize.height;
            int x = (-yw * imageSize.width / worldSize.width) + imageSize.width / 2;

            // find enclosing rectangle
            top = top<y ? top : y;
            left = left<x ? left : x;
            bottom = bottom>y ? bottom : y;
            right = right>x ? right : x;

            // draw individual point
            cv::circle(topviewImg, cv::Point(x, y), 4, currColor, -1);
        }

        // draw enclosing rectangle
        cv::rectangle(topviewImg, cv::Point(left, top), cv::Point(right, bottom),cv::Scalar(0,0,0), 2);

        double med = 0;
        if (lidarX.size() > 0) {
             std::sort(lidarX.begin(), lidarX.end());
             long medIndex = floor(lidarX.size() / 2.0);
             med = lidarX.size() % 2 == 0 ? (lidarX[medIndex - 1] + lidarX[medIndex]) / 2.0 : lidarX[medIndex];
        }

        // augment object with some key data
        char str1[200], str2[200];
        sprintf(str1, "id=%d, #pts=%d", it1->boxID, (int)it1->lidarPoints.size());
        putText(topviewImg, str1, cv::Point2f(left-250, bottom+50), cv::FONT_ITALIC, 1, currColor);
        sprintf(str2, "xmin=%2.2f m, xmed=%2.2f m, yw=%2.2f m", xwmin, med, ywmax-ywmin);
        putText(topviewImg, str2, cv::Point2f(left-250, bottom+125), cv::FONT_ITALIC, 1, currColor);
    }

    // plot distance markers
    float lineSpacing = 2.0; // gap between distance markers
    int nMarkers = floor(worldSize.height / lineSpacing);
    for (size_t i = 0; i < nMarkers; ++i)
    {
        int y = (-(i * lineSpacing) * imageSize.height / worldSize.height) + imageSize.height;
        cv::line(topviewImg, cv::Point(0, y), cv::Point(imageSize.width, y), cv::Scalar(255, 0, 0));
    }

    // display image
    string windowName = "3D Objects";
    cv::namedWindow(windowName, 1);
    cv::imshow(windowName, topviewImg);

    if(bWait)
    {
        cv::waitKey(0); // wait for key to be pressed
    }
}


// associate a given bounding box with the keypoints it contains
void clusterKptMatchesWithROI(BoundingBox &boundingBox, std::vector<cv::KeyPoint> &kptsPrev, std::vector<cv::KeyPoint> &kptsCurr, std::vector<cv::DMatch> &kptMatches)
{
    // Before a TTC estimate can be computed in the next exercise, you need to find all keypoint matches that belong to each 3D object.
    // You can do this by simply checking wether the corresponding keypoints are within the region of interest in the camera image.
    // All matches which satisfy this condition should be added to a vector.
    // The problem you will find is that there will be outliers among your matches.
    // To eliminate those, I recommend that you compute a robust mean of all the euclidean distances between keypoint matches and then remove those that are too far away from the mean.

    // The task is complete once the code performs as described and adds the keypoint correspondences to the "kptMatches" property of the respective bounding boxes.
    // Also, outlier matches have been removed based on the euclidean distance between them in relation to all the matches in the bounding box.
    std::vector<double> distances;
    std::vector<cv::DMatch> bbMatches;
    for (auto kptMatch: kptMatches) {
	    cv::Point kpPrev = kptsPrev[kptMatch.queryIdx].pt;
	    cv::Point kpCurr = kptsCurr[kptMatch.trainIdx].pt;
	    if (boundingBox.roi.contains(kpCurr)) {
		    double distance = fabs(cv::norm(kpCurr - kpPrev));
		    distances.push_back(distance);
		    bbMatches.push_back(kptMatch);
	    }
    }

    double med = median(distances);
    float threshold = 0.5;
    std::vector<cv::KeyPoint> keypointFiltered;
    std::vector<cv::DMatch> kptMatchesFiltered;
    for (int i = 0; i < distances.size(); ++i) {
	if (fabs(distances[i] - med) < (med * threshold)) {
                boundingBox.kptMatches.push_back(bbMatches[i]);
	}
    }
}


// Compute time-to-collision (TTC) based on keypoint correspondences in successive images
void computeTTCCamera(std::vector<cv::KeyPoint> &kptsPrev, std::vector<cv::KeyPoint> &kptsCurr, 
                      std::vector<cv::DMatch> kptMatches, double frameRate, double &TTC, cv::Mat *visImg)
{
    // compute distance ratios between all matched keypoints
    vector<double> distRatios; // stores the distance ratios for all keypoints between curr. and prev. frame
    for (auto it1 = kptMatches.begin(); it1 != kptMatches.end() - 1; ++it1)
    { // outer kpt. loop
        // get current keypoint and its matched partner in the prev. frame
        cv::KeyPoint kpOuterCurr = kptsCurr.at(it1->trainIdx);
        cv::KeyPoint kpOuterPrev = kptsPrev.at(it1->queryIdx);

        for (auto it2 = kptMatches.begin() + 1; it2 != kptMatches.end(); ++it2)
        { // inner kpt.-loop

            double minDist = 100.0; // min. required distance

            // get next keypoint and its matched partner in the prev. frame
            cv::KeyPoint kpInnerCurr = kptsCurr.at(it2->trainIdx);
            cv::KeyPoint kpInnerPrev = kptsPrev.at(it2->queryIdx);

            // compute distances and distance ratios
            double distCurr = cv::norm(kpOuterCurr.pt - kpInnerCurr.pt);
            double distPrev = cv::norm(kpOuterPrev.pt - kpInnerPrev.pt);

            if (distPrev > std::numeric_limits<double>::epsilon() && distCurr >= minDist)
            { // avoid division by zero

                double distRatio = distCurr / distPrev;
                distRatios.push_back(distRatio);
            }
        } // eof inner loop over all matched kpts
    }     // eof outer loop over all matched kpts

    // only continue if list of distance ratios is not empty
    if (distRatios.size() == 0)
    {
        TTC = NAN;
	cout << "\t CTTC: NAN\n";
        return;
    }

    // STUDENT TASK (replacement for meanDistRatio)
    double medDistRatio = median(distRatios);

    float dT = 1 / frameRate;
    TTC = -dT / (1 - medDistRatio);
    // EOF STUDENT TASK
}


void computeTTCLidar(std::vector<LidarPoint> &lidarPointsPrev,
                     std::vector<LidarPoint> &lidarPointsCurr, double frameRate, double &TTC)
{
    // auxiliary variables
    //double dT = 0.1; // time between two measurements in seconds
    double dT = 1 / frameRate;

    std::vector<double> prevX;
    for (auto px: lidarPointsPrev) {
	prevX.push_back(px.x);
    }
    std::vector<double> currX;
    for (auto px: lidarPointsCurr) {
	currX.push_back(px.x);
    }
    double prevXMedian = median(prevX);
    double currXMedian = median(currX);

    // find closest distance to Lidar points 
    double minXPrev = 1e9, minXCurr = 1e9;
    TTC = (currXMedian * dT) / (prevXMedian - currXMedian);
}


void matchBoundingBoxes(std::vector<cv::DMatch> &matches, std::map<int, int> &bbBestMatches, DataFrame &prevFrame, DataFrame &currFrame)
{
// Use keypoint matches between the previous and current image
// Make outer loop of matches. Find out by which bounding box keypoints are enclosed both on the previous and in the current frame
// These are potential match candidates whose box id's can be stored in multimap.
// Once loop for keypoint matches is complete, find all the matched candidates in the multimap which share the same bounding box id in the previous frame and count them.
// Associate the bounding boxes with the highest number of occurrences. Store box IDs of all matched pairs in map
	std::multimap<int, int> prevIDCurrID;
	for (auto match : matches) {
		cv::KeyPoint kp = prevFrame.keypoints[match.queryIdx];
		int prevBB = -1;
		int currBB = -1;
		for (auto &bb : prevFrame.boundingBoxes) {
			if ( bb.roi.contains(kp.pt) ) {
				prevBB = bb.boxID;
			}
		}
		for (auto &bb : currFrame.boundingBoxes) {
			if ( bb.roi.contains(kp.pt) ) {
				currBB = bb.boxID;
			}
		}
		if (prevBB != -1 and currBB != -1) {
			prevIDCurrID.emplace(prevBB, currBB);
		}
	}
	// https://stackoverflow.com/questions/2488941/find-which-numbers-appears-most-in-a-vector
	for (auto const &bb : prevFrame.boundingBoxes) {
		//std::vector<int> currBoxes;
		std::map<int, int> freq;
		std::pair <std::multimap<int,int>::iterator, std::multimap<int,int>::iterator> ret;
                ret = prevIDCurrID.equal_range(bb.boxID);
		int max_val = 0;
		int most_common = -1;
		for (std::multimap<int,int>::iterator it = ret.first; it != ret.second; ++it) {
			auto search = freq.find(it->second);
			if (search != freq.end()) {
				search->second++;
				if (search->second > max_val) {
					max_val = search->second;
					most_common = search->first;
				}
			}
			else {
				freq.emplace(std::make_pair(it->second, 1));
				if (1 > max_val) {
					max_val = 1;
					most_common = search->first;
				}
			}
		}
		if (most_common != -1) {
			bbBestMatches.emplace(std::make_pair(bb.boxID, most_common));
		}
	}
}
